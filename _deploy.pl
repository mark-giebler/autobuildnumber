#!/usr/bin/perl
#
# Author: Mark Giebler
# 2013-05-12
# This script uses <LF> as EOL. On Windows; Use appropriate editor to edit; e.g. Notepad++ or Geany
# This script updates a C Header file with version and incrementing build number.
# Based on: http://embeddedfun.blogspot.com/2012/11/auto-incrementing-build-number-in-mplabx.html
# but I wrote mine in Perl so it is portable between Windblows and Linux.
#
# The latest version of my script is available here:
#	https://gitlab.com/mark-giebler/autobuildnumber
#
# v1.16
#
# This script is to be run AFTER the end of a successful build.
# - It is Kicked off by the Makefile
# - It bumps the build number in the .h file that contains the version and build number macros.  (normally version.h)
# - Eclipse ARM projects only:
#		It copies the build artifacts to a BuildOutput directory.
#		Build artifact base name provided as command line argument.
#		e.g  perl _deploy.pl myBuild
#		It will copy if they exist: myBuild.elf, myBuild.bin, myBuild.srec, myBuild.hex
# - PIC Projects only:
#		It copies the latest hex file to the ./hex directory with a name that includes the version+build number.

# On a Windblows OS, use Strawberry Perl: http://strawberryperl.com/releases.html
#
# To run it manually @ cmd prompt:  perl _deploy.pl
#
use strict;
use feature qw(say);	# use say() to add automatic newline to string output
use File::Copy;

my $versionfile = "../Src/sw_version.h";	# the header file in which to bump the build number
my $buildnum=0;
my $buildnum_new=0;
my $versionMinor = 0;
my $versionMajor = 1;
my $version="0.0";
my $vbstring;
my $filedata;
# command line arg processing vars
my $totalArgs = $#ARGV + 1;
# get script name
my $scriptName = $0;

# BEGIN  Eclipse ARM specifics -------------------------------------------------
my $dstOutputPfn;			# path to where to copy build output from the last build
$dstOutputPfn = "../BuildOutput/";
my $artifactBaseName;
if ($totalArgs > 0 ) {
	$artifactBaseName = $ARGV[0];
}
# END    Eclipse ARM specifics -------------------------------------------------

# BEGIN  PIC project specifics -------------------------------------------------
=pod

my $projectnm = "pic18_c_RFsensor.X";	# used to name the output hex file
my $srchexpfn;			# path and file name of hex file output from the last build
$srchexpfn = "./dist/XC8_18F26J13_PKT3/production/$projectnm.production.hex";
my $dsthexpfn;			# path and file name of hex file to create with ver+build number in the name.
my $srcmappfn;			# path and file name of map file output from the last build
$srcmappfn = "./dist/XC8_18F26J13_PKT3/production/$projectnm.production.map";
my $dstmappfn;			# path and file name of map file to create with ver+build number in the name.
;
=cut
# END   PIC project specifics --------------------------------------------------
print "\n";
say "===============================================================";
say "Starting: $scriptName " ;
say "Author: Mark Giebler";
say "===============================================================";

# find version and build strings in ?.h file and bump build.
print "\n ... Bump build number in $versionfile ...\n";

my $fhandle;
open( $fhandle,"+<$versionfile")
	or die "Can't open $versionfile for update: $!\n";
# read line by line from version.h file
while(<$fhandle>) {
	if (/define BUILD_NUMBER_HEX/) {
		if(m/0x0(\d+)/)
		{
			$buildnum = $1;

			$buildnum_new = $buildnum + 1;
			say " BUILD_NUMBER_HEX : 0x0$buildnum -> 0x0" . $buildnum_new;

			s/0x0(\d+)/0x0$buildnum_new/;
			#say " BUILD_NUMBER_HEX : $_";
		}
    }elsif (/define BUILD_NUMBER_DEC/) {
		if(m/(\d+)/)
		{
			s/(\d+)/(($buildnum_new))/ge;
		}
		say " BUILD_NUMBER_DEC : $buildnum_new";
    }elsif (/define VERSION_MAJOR_NUMBER_DEC /) {
		if(m/(\d+)/)
		{
			$versionMajor = $1;
		}
	}elsif ( /define VERSION_MINOR_NUMBER_DEC/) {
		if(m/(\d+)/)
		{
			$versionMinor = $1;
		}

	}
	# add the current line with any changes from above
    $filedata .= $_;
}
# re-write the file with updates
seek($fhandle, 0, 0);
print $fhandle $filedata;
close( $fhandle );
if ($versionMinor > 9){
	$version= $versionMajor . "." . $versionMinor;
}else{
	$version= $versionMajor . ".0" . $versionMinor;
}
$vbstring= $version . "." . $buildnum_new ;
# BEGIN  Eclipse ARM specifics -------------------------------------------------
# for Eclipse ARM project
# Move and rename build artifacts to buildOutput directory ($dstOutputPfn)
#  v$version.$buildnum__$artifactBaseName
#print "Total args passed to $scriptName : $totalArgs\n";

print "Args: ".@ARGV."  Build Artifact : [$artifactBaseName]   Build Output path : [$dstOutputPfn]\n";

if ($totalArgs > 0 ) {
	my $filename = "";
	my $copyfail = "";
	my @types = (".bin",".elf",".map",".srec",".hex");
	my $dstPfn = $dstOutputPfn . "v$version.$buildnum" . "__" . $artifactBaseName;
	# now do copy and rename artifact files; bin, elf, map, srec
	foreach my $type (@types) {
		# delete old files
		unlink glob($dstOutputPfn . "v*" . $artifactBaseName . $type);
		$filename = $artifactBaseName . $type;
		if ( -f $filename ) {
			copy( $artifactBaseName . $type, $dstPfn . $type) or  $copyfail = 1;
			print " $artifactBaseName$type copy failed : $^E\n" unless !$copyfail;
			print " copied $artifactBaseName$type to " . $dstPfn . $type . "\n"  unless $copyfail;
		}else
		{
			print " file:  $filename does not exists, skipping it...\n";
		}
	}
}else {
	print " WARNING: skipping output artifact coping since no input arguments provided."
}
# END    Eclipse ARM specifics -------------------------------------------------

# comment out below for non PIC project using =pod/=cut.
=pod

# BEGIN  PIC MPLAB X IDE specifics ---------------------------------------------

say ' ... Copy and rename hex & map files ...';

say " src hex : $srchexpfn";
$dsthexpfn = "./hex/" . $projectnm . "_v" . $version. "." .$buildnum . ".hex";
say " dst hex : $dsthexpfn";
my $copyfail = "";
# now do copy and rename hex file
copy( $srchexpfn,$dsthexpfn) or  $copyfail = 1;
print " hex copy failed : $^E\n" unless !$copyfail;
;
say " src map : $srcmappfn";
$dstmappfn = "./hex/" . $projectnm . "_v" . $version. "." .$buildnum . ".map";
say " dst map : $dstmappfn";
my $copyfail = "";
# now do copy and rename map file
copy( $srcmappfn,$dstmappfn) or  $copyfail = 1;
print " map copy failed : $^E\n" unless !$copyfail;
;
# END    PIC MPLAB X IDE specifics ---------------------------------------------

=cut
# ----------------------------------------------
print "\n";
say "==================================================================";
say "Updated $versionfile  for next build: $buildnum_new  ";
say "Done Building: v$version.$buildnum";
#say "Updated $versionfile  to: $vbstring  " . ($copyfail ? "&  Copy Hex or Map file FAILED!" : "&  Copied HEX & Map files  ");
say "==================================================================";

;

# use =pod =cut pairs to comment out blocks of gunk.
# This gunk is an example of the format the version.h file needs to be in for above code to work.
;
=pod


/* version.h  by: Mark Giebler                                                        */
#ifndef	_VERSION_H
#define	_VERSION_H
/* ********************************************************************************** */
/* do not modify the build number macro, it is changed automatically during the build */
/* It is modified automatically during project build by:                              */
/*     _deploy.pl                                                                     */
/*  you can edit just these next two lines only!                                      */
#define VERSION_MAJOR_NUMBER_DEC 1		/*  you can edit just this line and           */
#define VERSION_MINOR_NUMBER_DEC 42		/*  you can edit this line only!              */
/* ********************************************************************************** */

/* ATTENTION: Do not edit below this line !!!!!                                       */
#define BUILD_NUMBER_HEX 0x01933
#define BUILD_NUMBER_DEC 1933

#define MAKE_HEX(h)	( ((h/10)*0x10) + (h%10) )

#define VERSION_MAJOR_NUMBER_HEX MAKE_HEX((VERSION_MAJOR_NUMBER_DEC))
#define VERSION_MINOR_NUMBER_HEX MAKE_HEX(VERSION_MINOR_NUMBER_DEC)

#define BUILD_DATE  __DATE__
#define BUILD_TIME  __TIME__
#endif	/* _VERSION_H */


=cut


# ================================
# End of File
# ================================
