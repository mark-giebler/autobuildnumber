/**
 * @file  sw_version.c
 * @brief declares the version info data in flash memory
 *
 *  Created on: 2013-05-21
 *  @author Mark Giebler
 */

#include "sw_version.h"

/* Private variables ---------------------------------------------------------*/
const char s_sw_version_prefix[] = "FW Version:";
const char s_sw_version[] =  VERSION_NUMBER_BUILD_STR;
const char s_sw_timestamp[] = BUILD_TIMESTAMP_STR;
const uint32_t sw_version_hex = VERSION_NUMBER_BUILD_UINT32_T;

/**
 * Get the version number for the application in BCD format.
 */
__attribute__ ((used)) const uint32_t get_sw_version_int(void)
{
	return sw_version_hex;
}
