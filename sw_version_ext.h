/**
 * @file sw_version_ext.h
 * @brief  This header declares the extern version strings
 *
 *  Created on: 2013-05-21
 *      @author Mark Giebler
 *
 */

#ifndef SW_VERSION_EXT_H_
#define SW_VERSION_EXT_H_
#include "sw_version.h"

/*  prefix string declared in sw_version.c source code - set it to the version text you want. e.g. "v:" or "Ver: " */
extern const char s_sw_version_prefix[];	//!< version string text prefix
extern const char s_sw_version[];			//!< version string numbers. Set to: VERSION_NUMBER_BUILD_STR
extern const char s_sw_timestamp[];			//!< date and time of the build. Set to: BUILD_TIMESTAMP_STR
extern const uint32_t sw_version_hex;		//!< 32 bit unsigned integer with the version number in BCD format. Set to: VERSION_NUMBER_BUILD_UINT32_T


const uint32_t get_sw_version_int(void);

#endif /* SW_VERSION_EXT_H_ */
